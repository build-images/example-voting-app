

# Using GitLab for CI/CD on Docker Swarm ( Play-With-Docker ) Environment.

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-generate-toc again -->
**Table of Contents**

- [Using GitLab for CI/CD on Docker Swarm ( & Cloud Foundry ) Environments.](#using-gitlab-for-cicd-on-docker-swarm---cloud-foundry--environments)
    - [Introduction](#introduction)
        - [Docker](#docker)
        - [Container Orchestrator](#container-orchestrator)
        - [Code Test & Deploy with GitLab](#code-test--deploy-with-gitlab)
    - [Create GitLab project for our application](#create-gitlab-project-for-our-application)
        - [The microservice docker voting application](#the-microservice-docker-voting-application)
        - [Project Description](#project-description)
    - [Deploy our application Manually](#deploy-our-application-manually)
        - [Play-with-Docker](#play-with-docker)
        - [Deploy Traefik Reverse Proxy on Docker Swarm](#deploy-traefik-reverse-proxy-on-docker-swarm)
        - [Build & Deploy our Application on Docker Swarm](#build--deploy-our-application-on-docker-swarm)
    - [Deploy our application automatically using Gitlab CI Pipelines](#deploy-our-application-automatically-using-gitlab-ci-pipelines)
        - [Fork the application](#fork-the-application)
        - [Configure GitLab with Play-with-Docker URL](#configure-gitlab-with-play-with-docker-url)
        - [Trigger your first Pipeline](#trigger-your-first-pipeline)
        - [The heart of the CI : gitlab-ci.yml](#the-heart-of-the-ci--gitlab-ciyml)
            - [image and services](#image-and-services)
            - [stages](#stages)
            - [Build a Push to Registry Job](#build-a-push-to-registry-job)
            - [Pull & deploy & test locally](#pull--deploy--test-locally)
            - [Deploy to Swarm](#deploy-to-swarm)
    - [To go Further](#to-go-further)
        - [Make use of GitLab Environments](#make-use-of-gitlab-environments)
        - [Configure a GitLab Runner](#configure-a-gitlab-runner)
    - [Conclusion](#conclusion)

<!-- markdown-toc end -->


## Introduction

As Project/Product teadm leader I have seen in few years that we where asked to accelerate new version delivery while increasing quality. Hopefully there is a development practice that has sucessfully optimize software project integration time and is known as [Continuous Integration (CI)](https://en.wikipedia.org/wiki/Continuous_integration). Developers integrate code into a shared repository frequently. Each code can then be verified by an automated process which can build & test the code. The key benefits of this process is that we can detect and fix issues quickly and easilly has if we commit often we commit only small changes. 

Additionally, [Continuous Delivery & Deployment](https://about.gitlab.com/2016/08/05/continuous-integration-delivery-and-deployment-with-gitlab/https://about.gitlab.com/2016/08/05/continuous-integration-delivery-and-deployment-with-gitlab/) has becomes best-practices for keeping applications deployables to production at any point or even automatically deployed to production environments, and allow us to achieve the goal of mooving fast while increasing the quality of code and tests to be checked automatically.

Glad for us, the open-source community was able to provide us tools that are very good helping us to release more frequently better quality code in an automated way.

### Docker

One of the first disruptive tool we discover is [Docker](https://www.docker.com/). I won't explain what is docker here, but for developers it was a gold bullet; they was able to uniquely and immutably package their application, whatever language they wanted to use and Operational then just needs to know how to run docker in a standard way.

### Container Orchestrator

Docker is a very great tool for developers. But while it's easy to run docker in production on a dedicated server in a old-fashion way, it tends to be seriously more difficult if we want to scale and become agnostic of the production servers (deploy to a cloud environment). 

Why would we ever wanted to do that ? It is always the same response: to be more efficient with the infrastructure, and decrease costs for both exploitation operations, and for the number of materials. Before the Cloud, let's say that we have 3 applications running on 2 datacenters, We used to have dedicated pools of servers for each application, on each datacenter. Everyone of those servers was sized to be used less than 30% of it's resource at a time, to be able to switch from 2 datacenters to 1 for operations or incidents. This means that we really don't optimize the cost of our infrastructure. And also, each deployments operations was executed unitarily on each server and even if we used configuration tools like ansible or puppet, it was quite using a lot of time to deploy our 2 datacenters. 

It is where using Container Orchestrator's such as [Docker Swarm](https://docs.docker.com/engine/swarm/), [Kubernetes](https://kubernetes.io/), [Mesos Marathon](https://mesosphere.github.io/marathon/), or also Platform as a service (PaaS) such as [Cloud Foundry](https://www.cloudfoundry.org/), [Openshift](https://www.openshift.com/) comes to the rescue and will allow project teams to easilly deploy their products without worried anymore with the underlying infrastructure. The orchestrator will be in charge of spawning and scale the containers, dispatching theme on the servers and activate network communications. I won't describe each of them here, but we are going to leverage in this tutorial **Docker Swarm** to deploy an example microservice application.

### Code Test & Deploy with GitLab

[GitLab](https://gitlab.com/) is a very powerfull swiss-army knife tool for managing projects/products. Gitlab offers an all in one way to centralized all informations: 

- The Code
- The IssueS for Evolutions and Bug tracking
- The Documentation
- The Tests
- The Deployments Scripts

And even we could Make uses of [GitLab CI](https://about.gitlab.com/gitlab-ci/) pipelines to automate all steps needed to push a version of our application to production such as :

- build the code
- make unitary tests
- deploy our code to a staging environment
- make full tests of our application and even interoperability tests with the rest of our ecosystem
- If all is OK we could even deploy to production
  - contact our internal control API to declare that we are going to deploy our code and check that their is no conflict operations 
  - deploy application to production
  - make tests to confirm deployment is ok if not, rollback

With a GitLab CI, we only write a single [YAML file](https://docs.gitlab.com/ce/ci/yaml/) to describe our process and what we wants to do at each steps, in a manner we control everything.

> With all that things combined together we have all the tools needed to produce more with better quality, for global product teams, it's well worth the learning curve investment.


## Create GitLab project for our application deployment tutorial

To be able to demonstrate the automatic deployment of the docker voting app to a docker swarm instance, I've set up a project on my gitlab account (it is forked from the [dockersamples](https://github.com/dockersamples/example-voting-app) github repo):

- https://gitlab.com/allamand/example-voting-app

> You can fork this project to follow this tutorial, this should take severals seconds.

!!! warning "Pre-requisite: You need a GitLab.com account in order to follow this tutorial"

### The microservice docker voting application

The docker voting application is composed of :

- A **Python** Webapp, which lets you vote between two options
- A **Redis** queue which collects new votes
- A **.NET** worker which consumes votes, and store them in..
- A **Postgres** database, backed by a Docker Volume
- A **Node.js** webapp which shows the results of the voting in real-time.

> On Top of that we will add a [Traefik Reverse Proxy](https://traefik.io) to dispach our Requests from internet to the vote and result services.

![](gitlab-vote-traefik-archi.png)

### Project Description

Before going further, let's take a minute to describe the content of the repo:

- **result** is the code source for the result application written in Node.js
- **vote** is the code source for the vote application written in Python
- **worker** is the code source for the worker written in .NET

We will use directly original docker image for our Redis, PostreSQL and Traefik containers.

We won't go more in details describing code of thoses services, which is out of scope of this tutorial, but let's say a note on Traefik Load Balancer.

Our application will have 2 services that must be exposes on the Internet, which are the Vote, and Result application. The Redis, the PostgreSQL and the worker will not be exposed on Internet and will be remain in a private network.

Since we deploy our application on a [Docker Swarm Mode](https://docs.docker.com/engine/swarm/) cluster, we will make use of [docker overlay network](https://docs.docker.com/engine/userguide/networking/get-started-overlay/#overlay-networking-and-swarm-mode) and create a public and private network.

The result and vote application will have a double attachment on public and private network, while redis, worker and DB will be only be attached to the private network.

The [Traefik](https://traefik.io/) Reverse Proxy will be listening only on the public Network and will be the only container that will expose ports to the Internet: it will be the entry point for all request to our application.

Traefik has the particularity that it is fully integrated with a docker swarm cluster. That means that each time we will start our vote our worker container, if we scale them up or down, or delete thems, then traefik will be automatically reconfigured without nothing to do. Traefik does this by listening to docker swarm events, and by searching at specific configuration labels that we will set-up for each container (we'll see that in docker-compose section)

> If you want more information on that you can follow [the traefik tutorial on play-with-docker labs](http://training.play-with-docker.com/traefik-load-balancing/)

## Deploy our application Manually

To demonstrate how we can combined, docker, gitlab, and docker swarm to deploy an application, we are going to uses the docker voting application microservice as an example.

> If you want you can skip the Manually part and directly goes to the [Deploy our application automatically using Gitlab CI Pipelines](#deploy-our-application-automatically-using-gitlab-ci-pipelines) section.

### Play-with-Docker

In order to allow everyone to follow (without the pre-requisite to already have a Docker Swarm cluster), I'll make usage of the great [Play With Docker](http://play-with-docker.com) plateforme.
As already said, this will allow us to have a Dedicated swarm cluster for our deployment, and we will show how to integrate this withing Gitlab.

Please, create a session on [Play With Docker](http://play-with-docker.com) plateforme.

> Note: this session will be available for 4 hours

When you go to the PWD interface and create a New session you'll get a page similar to :

![](gitlab-pwd-interface.png)

You can click `ADD NEW INSTANCE` in order to create an instance

Initiate your swarm cluster either by typing the following command :

```
docker swarm init --advertise-addr eth0
```

You then should have a swarm cluster initiate with one node (you can scale up to 5 nodes on PWD).


Please, now checkout my example repository, or your's if you have already forked it :

```
git clone https://gitlab.com/allamand/example-voting-app.git
cd example-voting-app
```


### Deploy Traefik Reverse Proxy on Docker Swarm

The first thing we will need to do, is to deploy the Traefik Reverse Proxy. We are using [Docker Stack](https://docs.docker.com/engine/swarm/stack-deploy/) to deploy our applications and [docker-compose v3](https://docs.docker.com/compose/compose-file/) file in order to describe the stack to deploy.

You can found the stack for traefik in the [traefik.yml](https://gitlab.com/allamand/example-voting-app/blob/master/traefik.yml) docker-compose file.

```docker-compose
version: '3'

services:

  traefik:
    image: traefik:1.3.4
    command: -l warning --docker --docker.swarmmode --docker.domain=docker.localhost --docker.watch --web
    ports:
      - '80:80'
      - '8080:8080'
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    networks:
      - net
    deploy:
      replicas: 1
      placement:
        constraints: [node.role == manager]
      restart_policy:
        condition: on-failure
      labels:
        traefik.enable: "false"


networks:
  net:
    driver: overlay

```

Whith this compose file we define the traefik service that we want to deploy. It will be using docker.swarmmode, and expose port 80 for incoming application request and port 8080 for Traefik Dashboard overview.

> Note, this setup is not to used for production, where you need a real swarm cluster, and enable TLS for all requests, but we are going to skip this step for this tutorial.

For communicating with docker swarm, traefik need to have the docker socket mounted in the container, and we added a deployment constraint so that the service will be deployed on a docker swarm manager node.

In this file we also create a network for this service.
To create the traefik stack (which will deploy this service and create the network) on the swarm you need to execute:

```
docker stack deploy traefik -c traefik.yml
```

we can check the status of the deployment using :

```
docker stack ls
docker stack ps traefik
```

> Once the deployment is OK, then because we are exposing 2 ports on the Swarm, PWD updated the page with 2 links such as :

![](gitlab-pwd-interface-ports.png)

You can click on the `8080` port link to access to the Traefik dashboard.

For now There is no other service, but we will come back after deploying our application.



### Build & Deploy our Application on Docker Swarm

We will uses similar approach to build and deploy the voting application using the [docker-compose.yml](https://gitlab.com/allamand/example-voting-app/blob/master/docker-compose.yml) file.

Here is an extract of this file defining the vote service:

```docker-compose
services:

  vote:
    build: ./vote
    image: ${REGISTRY_SLASH-sebmoule/}vote_vote${COLON_TAG-:latest}
    command: python app.py
    depends_on:
      - redis
    deploy:
      replicas: 1
      labels:
        - "traefik.backend=vote"
        - "traefik.port=80"
        - "traefik.frontend.rule=PathPrefixStrip:/vote"
        - "traefik.docker.network=traefik_net"
    networks:
     - traefik_net
     - private

```

- **build** the build part of the service definition tells docker-compose that in order to build this docker image it needs to Execute the `Dockerfile` present in the `vote` directory
- **image** When building the image, docker-compose will store the image in the `${REGISTRY_SLASH}vote_vote{COLON_TAG}` image name.
    - We uses the variables REGISTRY_SLASH and COLON_TAG in order to be able to change image name we are going to build with environment variables.

For that we just need to set the 2 environment variables example: 

- export REGISTRY_SLASH=myregistry.com/
- export COLON_TAG=:v1.0.0

> since we are now building manually, we won't push the images to a specific registry, and we do not need to set thoses 2 variables so the image name will be the default : `sebmoule/vote_vote:latest`


```
docker-compose build
```

You may be able to see the builded images with `docker images`

```
[node1] (local) root@10.0.35.3 ~/example-voting-app
$ docker images
REPOSITORY             TAG                 IMAGE ID            CREATED              SIZE
sebmoule/vote_result   latest              7fdb6475d896        40 seconds ago       229MB
sebmoule/vote_vote     latest              7e06d330091f        About a minute ago   84.7MB
sebmoule/vote_worker   latest              6d6083af8cd6        About a minute ago   635MB
traefik                <none>              dbcae52bfdec        12 days ago          45MB
python                 2.7-alpine          35084243e776        3 weeks ago          72MB
microsoft/dotnet       1.0.0-preview1      5be9548822f7        10 months ago        576MB
node                   5.11.0-slim         cb888ea932ad        15 months ago        207MB
```


- **labels** - The others particularity we have found in our docker-compose file, is the uses of specific deployment labels preffix with `traefik` :

```
      labels:
        - "traefik.backend=vote"
        - "traefik.port=80"
        - "traefik.frontend.rule=PathPrefixStrip:/vote"
        - "traefik.docker.network=traefik_net"
```

Thoses labels, will be attached to the container in the swarm and will be read by Traefik.
In this example, it tells traefix to proxify every request comming with the `/vote` URI to the `vote:80` backend, using the `traefik_net` docker network. Thanks to docker swarm mode, the traefik container will be able to contact our vote containers using the internal DNS by just calling `vote:80`.

- **networks** - this container has a double network attachment to the traefik_net which is our public network, and the private network which will not be exposed to internet directly.


To deploy the Application Stack :

```
docker stack deploy vote -c docker-compose-gitlab.yml
```

we can see the status of the deployment using :

```
docker stack ls
docker stack ps vote
```

This command may take a few time to achieve while docker needs to download missing docker images for redis and postgres.
You should see that while the swarm `Desired State` is Running, the `Current State` may be Preparing. Just wait few seconds for the state to becomes Running.

```
ID                  NAME                IMAGE                         NODE                DESIRED STATE       CURRENT STATE              ERROR               PORTS
lbux68xct1o4        vote_worker.1       sebmoule/vote_worker:latest   node1               Running             Preparing 9 seconds ago
f9c7hsvbzk40        vote_redis.1        redis:alpine                  node1               Running             Preparing 9 seconds ago
ikjz83k386hs        vote_vote.1         sebmoule/vote_vote:latest     node1               Running             Preparing 10 seconds ago
nybaxgy3ol9p        vote_result.1       sebmoule/vote_result:latest   node1               Running             Preparing 10 seconds ago
dkn3er3uscsr        vote_db.1           postgres:9.4                  node1               Running             Preparing 11 seconds ago
```

> This times, we don't have added ports exposed to the swarm, so that there is no ports that have pop-up in the pwd UI.

To access to our service, click on the `80` link in the PWD UI, and you should have a `404 page not found`. This is because we have configured our traefik reverse proxy to route requests based on the URI.

please add `/vote` at the end of the url this would be something like :

http://pwd10-0-35-3-80.host2.labs.play-with-docker.com/vote

and you should visualize the application :

![](gitlab-pwd-cats.png)

make your vote, and swith to the `/result` and you should see that it has taken into account your vote!!

![](gitlab-vote-result.png)



## Deploy our application automatically using Gitlab CI Pipelines

OK. Now that we have done all thoses thinks manually, it would be great if GitLab can automate thoses thinks to us each time we push some code into our repo. Let's see how to make this happen.

Remember that our Goal is to release more often to production with a better quality. [GitLab CI](https://about.gitlab.com/features/gitlab-ci-cd/) allows us to have only one tool full integrated to manage our code, and to execute a [pipeline](https://docs.gitlab.com/ee/ci/pipelines.html) at each commit wich launch automated build, execute our tests to discover issues as early as possible, and if this is successful can deploy to a target environment, which in our case will be docker swarm. 

### Fork the application

If you havn't already done, please fork my demo project :

- https://gitlab.com/allamand/example-voting-app

Because we are going to uses GitLab to automate the build, we will also uses [GitLab docker registry](https://about.gitlab.com/2016/05/23/gitlab-container-registry/) to store our images.


### Configure GitLab with Play-with-Docker URL

Please, create a new fresh session on [Play With Docker](http://play-with-docker.com) plateforme.

This time we won't create instances manually from the PWD Web Interface, but we are going to make uses of [docker-machine](https://docs.docker.com/machine/) and the PWD driver [docker-machine-driver-pwd](https://github.com/play-with-docker/docker-machine-driver-pwd), all within our GitLab Continuous Integration Pipeline!

The Goal of docker-machine is to create within a command a Virtual Machine configured to be able to run docker. With Docker-Machine, it will not pop-up a real VM, but instead will popup a docker-in-docker container which will emulate the traditional VM.

Retrieve your **PWD URL session** link which must be something like :

- http://host1.labs.play-with-docker.com/p/7e0b54a1-9475-4aca-b6a0-a9ee3829aa9e

and we are going to store this as a variable to our GitLab Pipeline.
On your project fork, go to `Settings/Pipelines` and scroll down to add a new variables :

![](gitlab-secret-var.png)

Than click `Add new variable` button.


### Trigger your first Pipeline

Now, in order to trigger a new build, make a change in a file for instance add a word in the README.md and commit the change.

This will trigger a new Pipeline :

![](gitlab-pipeline.png)

After few minutes, you can see that an instance must have pop-up in your PWD Interface, and that ports 80 and 8080 are now exposed (as when we done it manually). 

If you click on the `80` link and add the `/result` you should see that all has been deployed automatically.

Wow!! in less than 5 minutes, the Gitlab Pipeline has been able to rebuild all our application, to store the docker images in the GitLab Registry (click on the `Registry` link in your GitLab UI to see the images the pipeline have created), it has launch our application within a GitLab Runner to execute tests, and then it has deployed our application to our Docker Swarm environment backed by Play With Docker.


### The heart of the CI : gitlab-ci.yml

Let's see how GitLab has been able to do all that, using a single description [YAML file](https://docs.gitlab.com/ce/ci/yaml/). This File will be used to define an unlimited number of [jobs](https://docs.gitlab.com/ce/ci/yaml/#jobs) that will be exectuted by [GitLab Runners](https://docs.gitlab.com/ee/ci/runners/README.html). For each Job, GitLAb will send the job to a Gitlab Runner to execute in a Docker Image. the **image** tag define which image docker to uses.

You can find the whole project [.gitlab-ci.yml file here](https://gitlab.com/allamand/example-voting-app/blob/master/.gitlab-ci.yml) and let's break it to explain each part: 

#### image and services

```yaml
image: registry.gitlab.com/build-images/docker:latest

services:
  - docker:dind
```

- The **image** define which image to uses for the jobs.
- The **services** defines additional docker images which are linked to the main container. In this example our main image will be linked to a docker in docker images which will allow us to use the docker daemon of the linked dind image using the environment variable `DOCKER_HOST='tcp://docker:2375'`. You can find more information in the [documentation](https://docs.gitlab.com/ce/ci/docker/using_docker_build.html#use-docker-in-docker-executor).

#### stages

```
stages:
  - build
  - test
  - deploy
```

Under the stage command we will define the order for each Jobs. Each Jobs which will be related for the same stage will be executed in parallel, while stages are triggered sequencially using the order defined. We can define as many stages as we need for our workflow

#### Build a Push to Registry Job

```
Build & Push to Registry:
  stage: build
  variables:
    REGISTRY_SLASH: "$CI_REGISTRY_IMAGE/"
    COLON_TAG: ":1.0.0"
  script:
    - echo "Using gitlab registry $REGISTRY_SLASH and $COLON_TAG"
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $REGISTRY_SLASH        
    - docker-compose -f docker-compose-gitlab.yml build
    - docker-compose -f docker-compose-gitlab.yml push
  except:
    - tags
  tags:
    - docker
    - pwd
```

- The `Build & Push to Registry` job goal is to build our service images using our `docker-compose-gitlab.yml` file previously used in the manual section. Each Job is related to a stage so that GitLab now in which order it has to lauch thems.

- The `CI_REGISTRY_IMAGE` is a specific GitLab variable containing the Registry adress for our project.

> for a complete list of pre-defined gitlab variable see the [variables documentation](https://docs.gitlab.com/ce/ci/variables/README.html#variables)

We make use of this variable in combinaison af a specific `gitlab-ci-token` in order to authenticate to the [Gitlab docker registry](https://about.gitlab.com/2016/05/23/gitlab-container-registry/).

In this step, we ask the runner to build our application's services docker images and store thems in the Gitlab Registry


![](gitlab-vote-registry.png)


#### Pull & deploy & test locally

```
#Rajouter les tests ;)
Pull & deploy & test locally:
  stage: test
  variables:
    REGISTRY_SLASH: "$CI_REGISTRY_IMAGE/"
    COLON_TAG: ":1.0.0"      
  script:
    - echo "Using gitlab registry $REGISTRY_SLASH and $COLON_TAG"    
    - docker swarm init || true # if already a swarm
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $REGISTRY_SLASH    
    - docker-compose -f docker-compose-gitlab-injob.yml pull
    - docker stack deploy vote -c docker-compose-gitlab-injob.yml
    - sleep 5
#YOU CAN MAKE YOUR TESTS HERE
    - docker-compose -p test-vote -f docker-compose-tests.yml run newman
    - docker-compose -p test-vote -f docker-compose-tests.yml down
    - docker stack rm vote
  artifacts:
    paths:
      - tests/newman
  except:
    - tags

```

This is a job where we would use a GitLab runner in a `test` stage in order to deploy our application in a sandbox environment, and to execute functional tests with it.

I usually uses docker-compose with specific testing image to validate an API or else. In this example I uses a docker with [newman](https://github.com/postmanlabs/newman-docker) in order to execture tests scénarios I have previously build using [Postman](https://www.getpostman.com/), and where I store the Postman collection and environment withing my Git repository.

If the tests are OK, I want Gitlab to proceed with the next step which is the deployment of my application on docker swarm

> At this stage, the tutorial foccus on deploy on swarm, and in the tuto I didn't set up tests for the application yet ;)

#### Deploy to Swarm

```
Deploy to Swarm:
  stage: deploy
  variables:
    REGISTRY_SLASH: "$CI_REGISTRY_IMAGE/"
    COLON_TAG: ":1.0.0"
  before_script:
    - export SHELL=/bin/bash
    - echo "Installation Client Docker Swarm Floccus"
#You need to set-up PWD_URL in the Settings of your Gitlab Pipeline variable
    - echo using $PWD_URL
    - docker-machine create -d pwd node1
    - eval `docker-machine env node1 --shell bash`
    - docker swarm init --advertise-addr eth0 || true
    - docker stack deploy -c traefik.yml traefik
    - docker stack ls
  script:
    - docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $REGISTRY_SLASH
    - docker-compose -f docker-compose-gitlab.yml pull
    - docker stack deploy vote --with-registry-auth -c docker-compose-gitlab.yml
  only:
    - master
  except:
    - tags
```

- What's new in this job is the docker-machine part. The Job retrieve the `$PWD_URL` from our GitLab Variables we defined earlier in the `Settings/Pipeline` section

- Then we uses `docker-machine create -d pwd node1` to create the Docker VM we want to deploy to.

> If you have a Docker Swarm of your own, it's easy to change this step just to configure the DOCKER_HOST variable to point to your cluster instead.

- We make uses of `docker-machine env node1` in order to configure the local docker client to talk with the docker daemon of that node1.
- We just need to create the swarm cluster and launch our stack.
- We also could have add here some validation tests to be sure our deployment is good to go.


## TroubleShoot

### Error in deployment Pipeline

If you got the following error in your Pipeline 

```
Creating machine...
Error creating machine: Error in driver during machine creation: Could not create instance Post http://host2.labs.play-with-docker.com:80/sessions/37e71e63-438e-4fe2-85e0-71cf5348b302/instances: dial tcp 34.206.199.2:80: i/o timeout <nil>
ERROR: Job failed: exit code 1
```

It is because your PWD_URL link is out-of-date. In this case, create a new [Play With Docker](http://play-with-docker.com) session, store the new URL in your `Setting/Pipelines/Secret variables` section then relaunch the deployment Job

> Remember that the PWD_URL link is only valid for 4 hours

## To go Further

### Make use of GitLab Environments

That's it, no real difficulty here, and If you have a fix Docker Swarm cluster it would even be easier, and while we could then predict the final URL that will be used by the application we could have leverage the powerful feature offers with [GitLab Environments](https://docs.gitlab.com/ce/ci/environments.html)

> Since with Play-with-docker we can't predict in advance the url of the service I don't have used thems in this tutorial.

### Configure a GitLab Runner

To Bypass the limitation that can have Gitlab.com shared runners (ot enough memory in our case of building the .Net service), you can quickly ask GitLab to use your Own [Gitlab Runner](https://docs.gitlab.com/ee/ci/runners/README.html). It can be wherever you wants since it can communicate in https with Gitlab.com.

Go to the `Settings/Pipeline` section you can create or enable a runner that will allow you to build and push docker images and that will be used to automate our PWD instances.

![](gitlab-runner.png)

Following the instruction on this page, you can deploy a runner, and associate it with tags to idenitfy. Then you will make use of the specific `tags` in the Job description in order to select your runner


## Conclusion

In this tutorial I tried to show you how it can be very easy to combined GitLab with a Docker Swarm Cluster to make a great [Continuous Delivery](https://en.wikipedia.org/wiki/Continuous_delivery) pipeline. 

Of course the uses of Play-with-docker for this tutorial makes this project not viable for real life I hope it was quite useful for eveyone to try it without having to set-up a docker swarm on your own.

Here at My company we uses severals pipelines similar with this demo-one in order to automatically fullfill each steps of our build and release process, from developpement to tests, and to deployments, using Gitlab workflow and benefits of GitLab tracability.
