
APP=voting-app-doc
PORT=32770

stop:
	docker stop $(APP) || true 

rm:stop
	docker rm $(APP) || true

serve:rm
	docker run --rm -ti \
	 --name $(APP) \
	-v ${PWD}:/src \
	-w /src \
	-p $(PORT):9000 \
	registry.gitlab.com/build-images/mkdocs:latest bash -c ' set -x; \
	    mkdir /mysite ;\
	    echo "##java -jar /plantuml.jar -tsvg -o /src/images/ /src/*.puml" ;\
	    mkdocs build -d /mysite ;\
	    cd /mysite ;\
	    python -m SimpleHTTPServer 9000 ;\
	    echo bash ;\
	'
